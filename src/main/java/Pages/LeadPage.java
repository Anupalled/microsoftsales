package Pages;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class LeadPage {
	public static WebDriver driver;
	@FindBy(xpath = "//span[text()='Leads']")
	public static WebElement leadsButton;
	
	@FindBy(xpath = "//span[text()='New']/ancestor::button") 
	public static WebElement createNewButton;
	
	@FindBy(xpath = "//h1[@title='New Lead']")
	public static WebElement leadHeader;
	
	@FindBy(xpath = "//input[@aria-label='Topic']")
	public static WebElement inputTopic;
	
	@FindBy(xpath = "//input[@aria-label='First Name']")
	public static WebElement inputFirstName;
	
	@FindBy(xpath = "//input[@aria-label='Last Name']")
	public static WebElement inputLastName;
	
	@FindBy(xpath = "//span[text()='Save and close this Lead.']/..")
	public static WebElement saveAndClose;
	
	
	//span[text()='Sales Activity Social Dashboard']
	//span[text()='Sales Hub']
	
	public static WebDriverWait wait;
	public LeadPage(WebDriver driver) {
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	
	
	public void createLead() throws InterruptedException {
		waitForElementToDisplay(leadsButton);
		leadsButton.click();
		waitForElementToDisplay(createNewButton);
		createNewButton.click();
		waitForElementToDisplay(leadHeader);
		inputTopic.clear();
		inputTopic.sendKeys("ahb");
		inputFirstName.clear();
		inputFirstName.sendKeys("dsjh");
		inputLastName.clear();
		inputLastName.sendKeys("palavi");
		saveAndClose.click();
		
		
	}
	public static void waitForElementToDisplay(WebElement element) {
		WebDriverWait wait=new WebDriverWait(driver,40);
		wait.until(ExpectedConditions.visibilityOf(element));
	}


	
}

