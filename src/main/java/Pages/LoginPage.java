package Pages;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class LoginPage {
	public static WebDriver driver;
	@FindBy(xpath = "//input[@type='email']")
	public static WebElement email;
	
	@FindBy(xpath = "//input[@name='passwd']") 
	public static WebElement password;
	
	@FindBy(xpath = "//input[@type='submit']")
	public static WebElement submitButton;
	
	@FindBy(id = "idSIButton9") 
	public static WebElement nextButton;
	
	@FindBy(xpath = "//div[text()='Sign in']") 
	public static WebElement signInHeader;
	
	
	public LoginPage(WebDriver driver) {
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	
	public void login() throws InterruptedException {
		waitForElementToDisplay(email);
		email.sendKeys("Ananya@palledananya.onmicrosoft.com");
		nextButton.click();
		Thread.sleep(3000);
		password.sendKeys("Welcome1*");
		Thread.sleep(3000);
		submitButton.click();
		
	}
	public static void waitForElementToDisplay(WebElement element) {
		WebDriverWait wait=new WebDriverWait(driver,40);
		wait.until(ExpectedConditions.visibilityOf(element));
	}

	
}

