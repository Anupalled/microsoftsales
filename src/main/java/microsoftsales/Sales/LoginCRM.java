package microsoftsales.Sales;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import Pages.LeadPage;
import Pages.LoginPage;
import Utility.BrowserFactory;

public class LoginCRM {
	WebDriver driver;
@Test
	public void loginApp() throws InterruptedException {
	driver=BrowserFactory.startApplication(driver, "Chrome", "https://login.microsoftonline.com/929e4d00-fcf8-4fb4-8b03-bc204d3c20dd/oauth2/authorize?client_id=00000007-0000-0000-c000-000000000000&response_mode=form_post&response_type=code+id_token&scope=openid+profile&state=OpenIdConnect.AuthenticationProperties%3dMAAAAJDZa6--5RHrj-fd0IFEgYdP_ryeArmj2d-mB4l4Z9PznnP_-lVNgJ60qT9p1VvlfAEAAAABAAAACS5yZWRpcmVjdDBodHRwczovL3BhbGxlZGFuYW55YS5jcm04LmR5bmFtaWNzLmNvbS9tYWluLmFzcHg%26RedirectTo%3dMAAAAJDZa6%252b%252b5RHrj%252bfd0IFEgYft%252bAn4XoMKSnpsmwVEJCTCs4Fssbmj%252fED1X9XcmuZeSWh0dHBzOi8vcGFsbGVkYW5hbnlhLmNybTguZHluYW1pY3MuY29tLw%253d%253d&nonce=637602109739419087.Yjk0OTZiNTgtYjg0NC00MzQxLTk3YmMtMjlmOWQzOWFmOWUyMWNlMDlkZDAtN2E5Ni00YWI5LWE0MWItZmNhYzMyMzQzMDRh&redirect_uri=https%3a%2f%2fpnq--indcrmlivesg604.crm8.dynamics.com%2f&max_age=86400");
	
	LoginPage loginpage=new LoginPage(driver);
	loginpage.login();
	LeadPage leadpage=new LeadPage(driver);
	leadpage.createLead();
	}
}
